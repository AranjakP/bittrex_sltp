/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bittrexsltp.classes;

/**
 *
 * @author Aranjak
 */
public class CurrencyBalance {
    
    private String Currency;
    private String Balance;
    private String Available;
    private String Pending;
    private String CryptoAddress;
    private Boolean Requested;
    private String Uuid;
    private String AccountId;
    private Boolean Updated;

    public String getAccountId() {
        return AccountId;
    }

    public void setAccountId(String AccountId) {
        this.AccountId = AccountId;
    }

    public Boolean getUpdated() {
        return Updated;
    }

    public void setUpdated(Boolean Updated) {
        this.Updated = Updated;
    }

    public Boolean getAutoSell() {
        return AutoSell;
    }

    public void setAutoSell(Boolean AutoSell) {
        this.AutoSell = AutoSell;
    }
    private Boolean AutoSell;
    
    public CurrencyBalance() {
        
    }

    public CurrencyBalance(String Currency, String Balance, String Available, String Pending, String CryptoAddress, Boolean Requested, String Uuid) {
        this.Currency = Currency;
        this.Balance = Balance;
        this.Available = Available;
        this.Pending = Pending;
        this.CryptoAddress = CryptoAddress;
        this.Requested = Requested;
        this.Uuid = Uuid;
    }
    
    public CurrencyBalance(String Currency, String Balance) {
        this.Currency = Currency;
        this.Balance = Balance;
    }

    public String getCurrency() {
        return Currency;
    }

    public void setCurrency(String Currency) {
        this.Currency = Currency;
    }

    public String getBalance() {
        return Balance;
    }

    public void setBalance(String Balance) {
        this.Balance = Balance;
    }

    public String getAvailable() {
        return Available;
    }

    public void setAvailable(String Available) {
        this.Available = Available;
    }

    public String getPending() {
        return Pending;
    }

    public void setPending(String Pending) {
        this.Pending = Pending;
    }

    public String getCryptoAddress() {
        return CryptoAddress;
    }

    public void setCryptoAddress(String CryptoAddress) {
        this.CryptoAddress = CryptoAddress;
    }

    public Boolean getRequested() {
        return Requested;
    }

    public void setRequested(Boolean Requested) {
        this.Requested = Requested;
    }

    public String getUuid() {
        return Uuid;
    }

    public void setUuid(String Uuid) {
        this.Uuid = Uuid;
    }
    
}
