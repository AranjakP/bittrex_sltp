/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bittrexsltp.classes;

/**
 *
 * @author user
 */
public class CreatedOrder {

    private String OrderId;
    private String MarketName;
    private String MarketCurrency;
    private String BuyOrSell;
    private String OrderType;
    private String Quantity;
    private String Rate;

    public CreatedOrder(String OrderId, String MarketName, String MarketCurrency, String BuyOrSell, String OrderType, String Quantity, String Rate) {
        this.OrderId = OrderId;
        this.MarketName = MarketName;
        this.MarketCurrency = MarketCurrency;
        this.BuyOrSell = BuyOrSell;
        this.OrderType = OrderType;
        this.Quantity = Quantity;
        this.Rate = Rate;
    }

    public String getOrderId() {
        return OrderId;
    }

    public void setOrderId(String OrderId) {
        this.OrderId = OrderId;
    }

    public String getMarketName() {
        return MarketName;
    }

    public void setMarketName(String MarketName) {
        this.MarketName = MarketName;
    }

    public String getMarketCurrency() {
        return MarketCurrency;
    }

    public void setMarketCurrency(String MarketCurrency) {
        this.MarketCurrency = MarketCurrency;
    }

    public String getBuyOrSell() {
        return BuyOrSell;
    }

    public void setBuyOrSell(String BuyOrSell) {
        this.BuyOrSell = BuyOrSell;
    }

    public String getOrderType() {
        return OrderType;
    }

    public void setOrderType(String OrderType) {
        this.OrderType = OrderType;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String Quantity) {
        this.Quantity = Quantity;
    }

    public String getRate() {
        return Rate;
    }

    public void setRate(String Rate) {
        this.Rate = Rate;
    }
    
    
}
