/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bittrexsltp.classes;

/**
 *
 * @author user
 */
public class C2LSServerResponce {
    private String status;
    private String message_to_clients;
//    private String app_status;
    private String premium;
    private String error;
    private String error_code;
    private String expired_date;

    public C2LSServerResponce(String status, String error, String error_code, String message_to_clients, String expired_date, String premium) {
        this.status = status;
        this.error = error;
        this.error_code = error_code;
        this.message_to_clients = message_to_clients;
        this.expired_date = expired_date;
        this.premium = premium;
    }
    
    public C2LSServerResponce(String status, String error, String error_code) {
        this.status = status;
        this.error = error;
        this.error_code = error_code;
    }

//    public C2LSServerResponce(String status, String message_to_clients, String app_status, String coin) {
//        this.status = status;
//        this.message_to_clients = message_to_clients;
//        this.app_status = app_status;
//        this.coin = coin;
//    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage_to_clients() {
        return message_to_clients;
    }

    public void setMessage_to_clients(String message_to_clients) {
        this.message_to_clients = message_to_clients;
    }

    public String getPremium() {
        return premium;
    }

    public void setPremium(String premium) {
        this.premium = premium;
    }

//    public String getCoin() {
//        return coin;
//    }
//
//    public void setCoin(String coin) {
//        this.coin = coin;
//    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getError_code() {
        return error_code;
    }

    public void setError_code(String error_code) {
        this.error_code = error_code;
    }

    public String getExpired_date() {
        return expired_date;
    }

    public void setExpired_date(String expired_date) {
        this.expired_date = expired_date;
    }
    
    
}
