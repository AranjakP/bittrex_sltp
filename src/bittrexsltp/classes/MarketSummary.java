/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bittrexsltp.classes;

/**
 *
 * @author user
 */
public class MarketSummary {

    private String MarketName;
    private String High;
    private String Low;
    private String Volume;
    private String Last;
    private String BaseVolume;
    private String TimeStamp;
    private String Bid;
    private String Ask;
    private Integer OpenBuyOrders;
    private Integer OpenSellOrders;
    private String PrevDay;
    private String Created;
    private Boolean DisplayMarketName;

    public MarketSummary(String MarketName) {
    }

    public MarketSummary(String MarketName, String High, String Low, String Volume, String Last, String BaseVolume, String TimeStamp, String Bid, String Ask, Integer OpenBuyOrders, Integer OpenSellOrders, String PrevDay, String Created, Boolean DisplayMarketName) {
        this.MarketName = MarketName;
        this.High = High;
        this.Low = Low;
        this.Volume = Volume;
        this.Last = Last;
        this.BaseVolume = BaseVolume;
        this.TimeStamp = TimeStamp;
        this.Bid = Bid;
        this.Ask = Ask;
        this.OpenBuyOrders = OpenBuyOrders;
        this.OpenSellOrders = OpenSellOrders;
        this.PrevDay = PrevDay;
        this.Created = Created;
        this.DisplayMarketName = DisplayMarketName;
    }

    public String getMarketName() {
        return MarketName;
    }

    public void setMarketName(String MarketName) {
        this.MarketName = MarketName;
    }

    public String getHigh() {
        return High;
    }

    public void setHigh(String High) {
        this.High = High;
    }

    public String getLow() {
        return Low;
    }

    public void setLow(String Low) {
        this.Low = Low;
    }

    public String getVolume() {
        return Volume;
    }

    public void setVolume(String Volume) {
        this.Volume = Volume;
    }

    public String getLast() {
        return Last;
    }

    public void setLast(String Last) {
        this.Last = Last;
    }

    public String getBaseVolume() {
        return BaseVolume;
    }

    public void setBaseVolume(String BaseVolume) {
        this.BaseVolume = BaseVolume;
    }

    public String getTimeStamp() {
        return TimeStamp;
    }

    public void setTimeStamp(String TimeStamp) {
        this.TimeStamp = TimeStamp;
    }

    public String getBid() {
        return Bid;
    }

    public void setBid(String Bid) {
        this.Bid = Bid;
    }

    public String getAsk() {
        return Ask;
    }

    public void setAsk(String Ask) {
        this.Ask = Ask;
    }

    public Integer getOpenBuyOrders() {
        return OpenBuyOrders;
    }

    public void setOpenBuyOrders(Integer OpenBuyOrders) {
        this.OpenBuyOrders = OpenBuyOrders;
    }

    public Integer getOpenSellOrders() {
        return OpenSellOrders;
    }

    public void setOpenSellOrders(Integer OpenSellOrders) {
        this.OpenSellOrders = OpenSellOrders;
    }

    public String getPrevDay() {
        return PrevDay;
    }

    public void setPrevDay(String PrevDay) {
        this.PrevDay = PrevDay;
    }

    public String getCreated() {
        return Created;
    }

    public void setCreated(String Created) {
        this.Created = Created;
    }

    public Boolean getDisplayMarketName() {
        return DisplayMarketName;
    }

    public void setDisplayMarketName(Boolean DisplayMarketName) {
        this.DisplayMarketName = DisplayMarketName;
    }
    
}
