/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bittrexsltp.classes;

/**
 *
 * @author user
 */
public class Market {
    private String BaseCurrency;
    private String BaseCurrencyLong;
    private String Created;
    private String IsActive;
    private String IsSponsored;
    private String LogoUrl;
    private String MarketCurrency;
    private String MarketCurrencyLong;
    private String MarketName;
    private Double MinTradeSize;
    private String Notice;

    public Market(String BaseCurrency, String BaseCurrencyLong, String Created, String IsActive, String IsSponsored, String LogoUrl, String MarketCurrency, String MarketCurrencyLong, String MarketName, Double MinTradeSize, String Notice) {
        this.BaseCurrency = BaseCurrency;
        this.BaseCurrencyLong = BaseCurrencyLong;
        this.Created = Created;
        this.IsActive = IsActive;
        this.IsSponsored = IsSponsored;
        this.LogoUrl = LogoUrl;
        this.MarketCurrency = MarketCurrency;
        this.MarketCurrencyLong = MarketCurrencyLong;
        this.MarketName = MarketName;
        this.MinTradeSize = MinTradeSize;
        this.Notice = Notice;
    }

    public String getBaseCurrency() {
        return BaseCurrency;
    }

    public void setBaseCurrency(String BaseCurrency) {
        this.BaseCurrency = BaseCurrency;
    }

    public String getBaseCurrencyLong() {
        return BaseCurrencyLong;
    }

    public void setBaseCurrencyLong(String BaseCurrencyLong) {
        this.BaseCurrencyLong = BaseCurrencyLong;
    }

    public String getCreated() {
        return Created;
    }

    public void setCreated(String Created) {
        this.Created = Created;
    }

    public String getIsActive() {
        return IsActive;
    }

    public void setIsActive(String IsActive) {
        this.IsActive = IsActive;
    }

    public String getIsSponsored() {
        return IsSponsored;
    }

    public void setIsSponsored(String IsSponsored) {
        this.IsSponsored = IsSponsored;
    }

    public String getLogoUrl() {
        return LogoUrl;
    }

    public void setLogoUrl(String LogoUrl) {
        this.LogoUrl = LogoUrl;
    }

    public String getMarketCurrency() {
        return MarketCurrency;
    }

    public void setMarketCurrency(String MarketCurrency) {
        this.MarketCurrency = MarketCurrency;
    }

    public String getMarketCurrencyLong() {
        return MarketCurrencyLong;
    }

    public void setMarketCurrencyLong(String MarketCurrencyLong) {
        this.MarketCurrencyLong = MarketCurrencyLong;
    }

    public String getMarketName() {
        return MarketName;
    }

    public void setMarketName(String MarketName) {
        this.MarketName = MarketName;
    }

    public Double getMinTradeSize() {
        return MinTradeSize;
    }

    public void setMinTradeSize(Double MinTradeSize) {
        this.MinTradeSize = MinTradeSize;
    }

    public String getNotice() {
        return Notice;
    }

    public void setNotice(String Notice) {
        this.Notice = Notice;
    }
    
    
}
