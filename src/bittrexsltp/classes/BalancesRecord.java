/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bittrexsltp.classes;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author user
 */
public class BalancesRecord {
    @JsonProperty("Balance")
    public CurrencyBalance Balance;

    public BalancesRecord(CurrencyBalance Balance) {
        this.Balance = Balance;
    }

    public CurrencyBalance getBalance() {
        return Balance;
    }

    public void setBalance(CurrencyBalance Balance) {
        this.Balance = Balance;
    }
    
}
