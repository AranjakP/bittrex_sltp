package bittrexsltp;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.HostServices;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class PreloaderController implements Initializable {

    @FXML
    private StackPane rootPane;

    String version = "0.1";
    private HostServices hostServices;

    public HostServices getHostServices() {
        return hostServices ;
    }

    public void setHostServices(HostServices hostServices) {
        this.hostServices = hostServices ;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        new Preloader().start();
    }

    class Preloader extends Thread {

        @Override
        public void run() {
            try {
                Thread.sleep(500);
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        Parent root = null;
                        try {
                            FXMLLoader loader = new FXMLLoader(getClass().getResource("BittrexSlTp.fxml"));
                            root = loader.load();
                            BittrexSlTpController controller = loader.getController();
                            controller.setHostServices(getHostServices());
                        } catch (IOException ex) {
                            Logger.getLogger(PreloaderController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        Scene scene = new Scene(root);
                        Stage stage = new Stage();
                        stage.setTitle("CC2LS Bittrex SLTP v" + version);
                        stage.setMinHeight(650);
                        stage.setMinWidth(920);
                        stage.setMaxHeight(650);
                        stage.setMaxWidth(920);
                        stage.setScene(scene);
                        rootPane.getScene().getWindow().hide();
                        stage.show();
                    }

                });

            } catch (InterruptedException ex) {
                Logger.getLogger(PreloaderController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
